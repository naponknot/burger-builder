import React from 'react';

import classes from './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const burger = (props) => {
    let transformedIngredients = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey} />;
            });
        })
        .reduce((prev, curr) => {
            return prev.concat(curr)
        }, []);

    const displayedIngredients = transformedIngredients.length === 0 ? <p>Please start adding ingredients!</p> : transformedIngredients;

    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {displayedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default burger;